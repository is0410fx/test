#include "ns3/animation-interface.h"
#include "ns3/applications-module.h"
#include "ns3/command-line.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-helper.h"
#include "ns3/olsr-module.h"
#include "ns3/string.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/yans-wifi-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("olsr-test");

int main (int argc, char *argv[]) {

  CommandLine cmd (__FILE__);
  cmd.Parse (argc, argv);

  int size = 25;

  NodeContainer nodes;
  nodes.Create (size);

  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0),
                                 "MinY", DoubleValue (0),
                                 "DeltaX", DoubleValue (100),
                                 "DeltaY", DoubleValue (100),
                                 "GridWidth", UintegerValue (sqrt(size)));
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (nodes);

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss("ns3::RangePropagationLossModel", "MaxRange", DoubleValue(100));
  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  wifiPhy.SetChannel (wifiChannel.Create ());
  wifiPhy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11);

  NqosWaveMacHelper wifiMac = NqosWaveMacHelper::Default ();

  Wifi80211pHelper wifi = Wifi80211pHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"),
                                "ControlMode", StringValue ("OfdmRate6MbpsBW10MHz"));

  NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, nodes);

  Ipv4StaticRoutingHelper staticRouting;
  OlsrHelper olsr;
  Ipv4ListRoutingHelper list;
  list.Add (staticRouting, 0);
  list.Add (olsr, 1000);
  InternetStackHelper stack;
  stack.SetRoutingHelper (list);
  stack.Install (nodes);

  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("1.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer interfaces = ipv4.Assign (devices);

  UdpServerHelper server (9);
  ApplicationContainer serverApp = server.Install (nodes.Get (size-1));
  serverApp.Start (Seconds (1.0));
  serverApp.Stop (Seconds (20.0));

  UdpClientHelper client (interfaces.GetAddress (size-1), 9);
  client.SetAttribute ("MaxPackets", UintegerValue (5));
  client.SetAttribute ("Interval", TimeValue (Seconds (1)));
  client.SetAttribute ("PacketSize", UintegerValue (512));

  ApplicationContainer clientApp = client.Install (nodes.Get (0));
  clientApp.Start (Seconds (12.0));
  clientApp.Stop (Seconds (20.0));

  Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ("olsr-test.routes", std::ios::out);
  olsr.PrintRoutingTableAllAt (Seconds (12), routingStream);

  LogComponentEnable ("UdpServer", LOG_LEVEL_INFO);

  Simulator::Stop (Seconds (20.0));

  AnimationInterface anim ("olsr-test.xml");

  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
