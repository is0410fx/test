#include "ns3/animation-interface.h"
#include "ns3/applications-module.h"
#include "ns3/command-line.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-helper.h"
#include "ns3/olsr-module.h"
#include "ns3/string.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/yans-wifi-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("vary-range");

int main (int argc, char *argv[]) {

  CommandLine cmd (__FILE__);
  cmd.Parse (argc, argv);

  NodeContainer node_A;
  node_A.Create (2);

  NodeContainer node_B;
  node_B.Create (2);

  MobilityHelper mobility;
  Ptr<ListPositionAllocator> position = CreateObject<ListPositionAllocator> ();
  position -> Add (Vector (0, 0, 0));
  position -> Add (Vector (1000, 0, 0));
  position -> Add (Vector (0, 500, 0));
  position -> Add (Vector (1000, 500, 0));
  mobility.SetPositionAllocator (position);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (node_A);
  mobility.Install (node_B);

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");

  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  wifiPhy.SetPcapDataLinkType(YansWifiPhyHelper::DLT_IEEE802_11);
  wifiPhy.SetChannel (wifiChannel.Create ());

  NqosWaveMacHelper wifiMac = NqosWaveMacHelper::Default ();

  Wifi80211pHelper wifi  = Wifi80211pHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"),
                                "ControlMode", StringValue ("OfdmRate6MbpsBW10MHz"));

  wifiPhy.Set("TxGain", DoubleValue (5));
  NetDeviceContainer netdevice_A = wifi.Install (wifiPhy, wifiMac, node_A);

  wifiPhy.Set("TxGain", DoubleValue (10));
  NetDeviceContainer netdevice_B = wifi.Install (wifiPhy, wifiMac, node_B);

  Ipv4StaticRoutingHelper staticRouting;
  OlsrHelper olsr;
  Ipv4ListRoutingHelper list;
  list.Add (staticRouting, 0);
  list.Add (olsr, 1000);

  InternetStackHelper stack;
  stack.SetRoutingHelper (list);
  stack.Install (node_A);
  stack.Install (node_B);

  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("1.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer interface_A = ipv4.Assign (netdevice_A);
  Ipv4InterfaceContainer interface_B = ipv4.Assign (netdevice_B);

  Simulator::Stop (Seconds (20.0));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}
