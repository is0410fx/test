#include <time.h>
#include "ns3/animation-interface.h"
#include "ns3/applications-module.h"
#include "ns3/command-line.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-helper.h"
#include "ns3/ns2-mobility-helper.h"
#include "ns3/olsr-module.h"
#include "ns3/string.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/wifi-80211p-helper.h"
#include "ns3/yans-wifi-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("main");

int main (int argc, char *argv[]) {

  int truck_num = 25;
  int car_num = 75;

  srand (time (NULL));
  int p = rand () % 10000;

  srand (p);
  int sender = rand () % car_num;
  srand (p * 2);
  int receiver = rand () % car_num;

  printf("car:%d, truck:%d, parameter:%d\n" , car_num, truck_num, p);

  std::string sumo;

  CommandLine cmd (__FILE__);
  cmd.AddValue ("sumo", "sumo", sumo);
  cmd.Parse (argc, argv);

  NodeContainer node_car;
  node_car.Create (car_num);

  Ns2MobilityHelper ns2 = Ns2MobilityHelper (sumo);
  ns2.Install ();

  NodeContainer node_truck;
  node_truck.Create (truck_num);

  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::RandomRectanglePositionAllocator",
                                       "X", StringValue ("ns3::UniformRandomVariable[Min=0|Max=1500]"),
                                       "Y", StringValue ("ns3::UniformRandomVariable[Min=0|Max=1500]"));
  mobility.SetMobilityModel ("ns3::RandomDirection2dMobilityModel",
                                   "Bounds", RectangleValue (Rectangle (0, 1500, 0, 1500)),
                                   "Speed", StringValue ("ns3::UniformRandomVariable[Min=1|Max=16.67]"));
  mobility.Install (node_truck);

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss("ns3::FriisPropagationLossModel");

  YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
  wifiPhy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11);
  wifiPhy.SetChannel (wifiChannel.Create ());

  NqosWaveMacHelper wifiMac = NqosWaveMacHelper::Default ();

  Wifi80211pHelper wifi = Wifi80211pHelper::Default ();
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode", StringValue ("OfdmRate6MbpsBW10MHz"),
                                "ControlMode", StringValue ("OfdmRate6MbpsBW10MHz"));

  wifiPhy.Set("TxGain", DoubleValue (-0.45531));
  NetDeviceContainer netdevice_car = wifi.Install (wifiPhy, wifiMac, node_car);
  wifiPhy.Set("TxGain", DoubleValue (2.72916));
  NetDeviceContainer netdevice_truck = wifi.Install (wifiPhy, wifiMac, node_truck);

  Ipv4StaticRoutingHelper staticRouting;
  OlsrHelper olsr;

  olsr.Set ("Willingness", StringValue ("default"));
  Ipv4ListRoutingHelper list_car;
  list_car.Add (staticRouting, 0);
  list_car.Add (olsr, 1000);
  InternetStackHelper stack_car;
  stack_car.SetRoutingHelper (list_car);
  stack_car.Install (node_car);

  olsr.Set ("Willingness", StringValue ("high"));
  Ipv4ListRoutingHelper list_truck;
  list_truck.Add (staticRouting, 0);
  list_truck.Add (olsr, 1000);
  InternetStackHelper stack_truck;
  stack_truck.SetRoutingHelper (list_truck);
  stack_truck.Install (node_truck);

  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("1.0.0.0", "255.255.255.0");
  Ipv4InterfaceContainer interface_car = ipv4.Assign (netdevice_car);
  Ipv4InterfaceContainer interface_truck = ipv4.Assign (netdevice_truck);

  UdpServerHelper server (9);
  ApplicationContainer Apps = server.Install (node_car.Get (receiver));
  Apps.Start (Seconds (0.0));
  Apps.Stop (Seconds (180.0));

  UdpClientHelper client (interface_car.GetAddress (receiver), 9);
  client.SetAttribute ("MaxPackets", UintegerValue (5));
  client.SetAttribute ("Interval", TimeValue (Seconds (20)));
  client.SetAttribute ("PacketSize", UintegerValue (512));

  Apps = client.Install (node_car.Get (sender));
  Apps.Start (Seconds (60.0));
  Apps.Stop (Seconds (180.0));

  LogComponentEnable ("UdpServer", LOG_LEVEL_INFO);
  Simulator::Stop (Seconds (180.0));
  Simulator::Run ();
  Simulator::Destroy ();

  return 0;
}

/*
[umo-file] ns3.32 + sumo1.4.0

cd sumo

netgenerate --grid --tls.guess --tls.guess.threshold 30 --output-file=sumo.net.xml --grid.number 7 --grid.length 250 --no-turnarounds

/home/sakai/sumo/tools/randomTrips.py -n sumo.net.xml -e 1.00 -p 0.01 -o sumo.trip.xml

duarouter -n sumo.net.xml -t sumo.trip.xml -o sumo.rou.xml

sumo -c sumo.sumocfg --fcd-output sumo.xml

cd tools

python traceExporter.py -i /home/sakai/sumo/sumo.xml --ns2mobility-output=/home/sakai/sumo/sumo.tcl --shift=1
*/
